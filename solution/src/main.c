#include <../include/image.h>
#include <../include/image_io.h>
#include <../include/rotate.h>
#include <stdio.h>
#include <string.h>

#define EXECUTABLE_NAME "./image-transformer"




int main( int argc, char** argv ) {
    if (argc != 3)
    {
        fprintf(stderr, "Enter input bmp file and output file.\nPattern: \"./image-transformer <source-image> <transformed-image>\"");
        return 1;
    }
    
    if (strcmp (argv[0], EXECUTABLE_NAME) == 0)
    {
         fprintf(stderr, "Incorrect executable_name.\nPattern: \"./image-transformer <source-image> <transformed-image>\"");
         return 1;
    }
    if (strcmp (argv[1], "") == 0)
    {
      fprintf(stderr, "Incorrect file name.\nPattern: \"./image-transformer <source-image> <transformed-image>\""); 
      return 1;  
    }
    FILE* file = fopen(argv[1], "rb");
    if (!file)
    {
        fprintf(stderr, "Can't find the file.\nPattern: \"./image-transformer <source-image> <transformed-image>\"");
        return 1;
    }
    struct image* img = (struct image*)malloc(sizeof(struct image));
    if (!img)
    {
        fprintf(stderr, "Unable to allocate memory for the image");
        fclose(file);
        return 1;
    }
    
    switch (from_bmp(file, img))
    {
    case READ_INVALID_DATA:
        fprintf(stderr, "Invalid Data");
    case READ_INVALID_HEADER:
        fprintf(stderr, "Invalid  bmp_header");
        break;
    case READ_INVALID_SIGNATURE:
        fprintf(stderr, "Invalid type of bmp file. Сheck the file for correctness");
        break;
    case READ_INVALID_BITS:
        fprintf(stderr, "Data cannot be read. Сheck the file for correctness");
        break;
    case READ_INVALID_MEMORY:
        fprintf(stderr, "It is impossible to allocate memory for an image data!");
        break;
    case READ_OK:       
        printf("The image was sucessfully read!\n");
        
        struct image* output_img = (struct image*) malloc(sizeof(struct image));
        if (!output_img)
    {
        fprintf(stderr, "Unable to allocate memory for the image");
        fclose(file);
        image_destroy(img);
        return 1;
    }
        output_img = rotate(output_img, img);
        if (!output_img)
        {
            fprintf(stderr, "Error when creating a rotated image");
            fclose(file);
            image_destroy(img);
            return 1;
        }
        
        FILE* output_file = fopen(argv[2], "wb");
        if(!output_file){
            fprintf(stderr, "Can't find the output_file.\nPattern: \"./image-transformer <source-image> <transformed-image>\"");
            fclose(file);
            image_destroy(img);
            image_destroy(output_img);
            return 1;
        }
        
        switch (to_bmp(output_file, output_img))
        {
        case WRITE_ERROR:
            fprintf(stderr, "Can't write data to file");
            fclose(output_file);
            image_destroy(output_img);
            break;
        case WRITE_OK:
            printf("The image was successfully rotated!\n");
            fclose(output_file);
            image_destroy(output_img);
            break;
        }
        break;
    
    }
    fclose(file);
    image_destroy(img);
    return 0;
}
