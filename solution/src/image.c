#include<../include/image.h>
#include <stdio.h>
//Data will always evaluate to 'true'
struct image* image_create(struct image* img, struct dimensions size){
    img->size = size; 
    struct pixel* data = (struct pixel*) malloc(sizeof(struct pixel) * size.width * size.height);
    if (!data)
    {
      free(img);
      return NULL;
    }
    
    img->data = data;
    return img;
    
    
}

void image_destroy(struct image* img){
  if (img){
    if(img->data){
    free(img->data);
    }
    free(img);
  }
}
