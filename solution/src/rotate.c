#include <../include/image.h>
#include <../include/rotate.h>
#include <stdio.h>
struct image* rotate(struct image* output_img, struct image* source ){
    if (output_img == NULL || source == NULL)
    {
        return NULL;
    }
    
    uint64_t width = source->size.width;
    uint64_t height = source->size.height;
    struct dimensions output_size = {.width = height, .height = width};
    output_img = image_create(output_img, output_size);
    if (!output_img){
        
        return NULL;
    }
    
    for (size_t i = 0; i < height; i++)
    {
        for (size_t j = 0; j < width; j++)
        {
            output_img->data[j * height + height - i - 1] = source->data[i*width + j];

        }
        
        
    }
    return output_img;
    
}
