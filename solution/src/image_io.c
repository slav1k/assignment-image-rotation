#include <../include/image.h>
#include <../include/image_io.h>
#define bfType_const 0x4D42
#define biSize_const 40
#define biPlanes_const 1
#define biBitCount_const 24
#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)


enum read_status read_data(struct image* img, FILE* in, uint32_t padding){
  if (img == NULL  || in == NULL)
  {
    return READ_INVALID_DATA;
  }
  
  for (size_t i = 0; i < img->size.height; i++)
  {

    if(fread(&img->data[i * img->size.width], sizeof(struct pixel) * img->size.width, 1 , in) != 1){
      return READ_INVALID_BITS;
    }
    if (fseek(in, padding, SEEK_CUR) != 0) {
      return READ_INVALID_BITS;
    }
  }
  return READ_OK;
  
}

enum write_status write_data(struct image* img, FILE* out, uint32_t padding){
   
  struct pixel px = {.b = 0, .g = 0, .r = 0};
   for (size_t i = 0; i < img->size.height; i++)
    {
      size_t write_row = fwrite(&img->data[ i * img->size.width] , sizeof(struct pixel)*img->size.width, 1, out);
        if (write_row != 1){
        
          return WRITE_ERROR;
        }

     if (fwrite(&px, (uint8_t)padding, 1, out) != 1){
        return WRITE_ERROR;
     }
       
    
  }
  return WRITE_OK;
}

uint32_t padding_solution(uint64_t width){
  return (uint32_t)(4 - (width * sizeof(struct pixel)) % 4 ) ;
}

enum read_status from_bmp(FILE* in, struct image* img){
  if (img == NULL || in == NULL)
  {
    return READ_INVALID_DATA;   
  }
  
  struct bmp_header bmp;
  
  if (fread(&bmp, sizeof(struct bmp_header), 1, in) != 1){
    return READ_INVALID_HEADER;
  }
  if (bmp.bfType != 0x4D42)
  {
    return READ_INVALID_SIGNATURE;
  }
  uint64_t width = (uint64_t)bmp.biWidth;
  
  uint32_t padding = padding_solution(width);
  uint64_t height = (uint64_t)bmp.biHeight;
  struct dimensions size = {.width = width, .height = height};
  img = image_create(img, size);
  if (!img)
  {
    return READ_INVALID_MEMORY;
  }
  enum read_status read_stat = read_data(img, in, padding);
  return read_stat;
}
enum write_status to_bmp(FILE* out, struct image* img){
    struct bmp_header bmp;
    bmp.bfType = bfType_const; 
    bmp.bfileSize = (uint32_t)(sizeof(struct bmp_header)) + (uint32_t)(img->size.width * img->size.height * sizeof(struct pixel));
    bmp.bfReserved = 0;
    bmp.bOffBits = sizeof(struct bmp_header);
    bmp.biSize = biSize_const;
    bmp.biWidth = (uint32_t)img->size.width;
    bmp.biHeight = (uint32_t)img->size.height;
    bmp.biPlanes = biPlanes_const;
    bmp.biBitCount = biBitCount_const;
    bmp.biCompression = 0;
    bmp.biSizeImage = 0;
    bmp.biXPelsPerMeter = 0;
    bmp.biYPelsPerMeter = 0;
    bmp.biClrUsed = 0;
    bmp.biClrImportant = 0;
   
    if(fwrite(&bmp, sizeof(struct bmp_header), 1, out) != 1){
      return WRITE_ERROR;
    }
    uint32_t padding = padding_solution(img->size.width);
    if (write_data(img, out, padding) == WRITE_OK)
    {
      return WRITE_OK;
    }
    return WRITE_ERROR;

}
