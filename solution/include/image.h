 #include <stdint.h>
 #include <stdlib.h>
 struct pixel { 
  uint8_t b, g, r; 
  };

struct dimensions {
  uint64_t width, height;
};

struct image {
  struct dimensions size;
  struct pixel* data;
};

struct image* image_create(struct image* img, struct dimensions size );
void image_destroy( struct image* img);
