#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct bmp_header;
  
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_MEMORY,
  READ_INVALID_DATA
  /* коды других ошибок  */
  };
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};


enum read_status read_data(struct image* img, FILE* in, uint32_t padding);
enum write_status write_data(struct image* img, FILE* out, uint32_t padding);
enum read_status from_bmp( FILE* in, struct image* img);
enum write_status to_bmp( FILE* out, struct image* img );
uint64_t padding(uint64_t width);
